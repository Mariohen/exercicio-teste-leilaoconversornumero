package br.com.Leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LanceTeste {

    private Lance lance;
    private Usuario usuario;

    @BeforeEach
    public void setupInicial(){
        int id = 2;
        String nome = "Mario";
        this.usuario = new Usuario(id, nome);

        this.lance = new Lance(usuario, 50.00);

    }

    @Test
    public void testarLanceSemNome(){
        Assertions.assertThrows(RuntimeException.class, ()-> {lance.validaEntradaLance();});
    }

    @Test
    public void testarLanceSemValor(){
        Assertions.assertThrows(RuntimeException.class, ()-> {lance.validaEntradaLance();});
    }

    @Test
    public void testarLanceOK(){}

}
