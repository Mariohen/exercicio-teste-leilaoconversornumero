package br.com.Leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LeiloeiroTeste {

    @Test
    public void TestarMaiorLance()

    {
        Usuario primeiroUsuario = new Usuario(55, "Eu");
        Usuario segundoUsuario = new Usuario(88, "Mario");
        Lance primeiroLance = new Lance(primeiroUsuario, 10.0);
        Lance segundoLance = new Lance(segundoUsuario, 20.0);
 ;
        Leilao leilao = new Leilao();
        Leiloeiro leiloeiro = new Leiloeiro("Henrique", leilao);

        leilao.adicionarNovoLance(primeiroLance);
        leilao.adicionarNovoLance(segundoLance);

        String nomeUsuarioMaiorLance = leiloeiro.retornarMaiorLance(); ;

        Assertions.assertEquals(segundoUsuario.getNome(),  nomeUsuarioMaiorLance);

    }


}
