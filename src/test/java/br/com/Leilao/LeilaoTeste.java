package br.com.Leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LeilaoTeste {

    private Usuario usuario;
    private Lance lance;
    private Leilao leilao;

    @BeforeEach
    public void setupInicial(){
        int id = 2;
        String nome = "Mario";
        this.usuario = new Usuario(id, nome);

        this.lance = new Lance(usuario, 50.00);
        this.leilao = new Leilao();
    }

    @Test
    public void TestarAdicionarNovoLance()
    {
        leilao.adicionarNovoLance(lance);
        Integer quantidadeLancesEsperada = 1;
        Assertions.assertEquals( quantidadeLancesEsperada, leilao.getListaLances().size());
    }

    @Test
    public void TestarAdicionarSemValorLance()
    {
        leilao.adicionarNovoLance(lance);
        Assertions.assertThrows(RuntimeException.class, ()-> {leilao.validarNovoLance(lance);});
    }
}
