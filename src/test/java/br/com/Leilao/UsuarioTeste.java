package br.com.Leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UsuarioTeste {

    private Usuario usuario;

    @BeforeEach
    public void setupUsuario(){
        int id = 2;
        String nome = "Mario";
        this.usuario = new Usuario(id, nome);
        }

    @Test
    public void testarUsuarioComIDZero(){
        Assertions.assertThrows(RuntimeException.class, ()-> {usuario.validaEntrada();});
    }

    @Test
    public void testarUsuarioSemNome(){
        Assertions.assertThrows(RuntimeException.class, ()-> {usuario.validaEntrada();});
    }

    @Test
    public void testarUsuarioOK(){

    }
}
