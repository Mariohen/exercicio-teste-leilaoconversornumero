package br.com.Leilao;

public class Leiloeiro {

    private String nome;
    private Leilao leilao;

    public Leiloeiro(String nome, Leilao lance) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public String retornarMaiorLance(){
        double valorInicial =0;
        String nomeUsuario = " ";
        for(int i=0; i < leilao.getListaLances().size(); i++){
            if(leilao.getListaLances().get(i).getValorDoLance() > valorInicial){
                valorInicial = leilao.getListaLances().get(i).getValorDoLance();
                nomeUsuario = leilao.getListaLances().get(i).getUsuario().getNome();
            }
        }
        return nomeUsuario;

    }
}
