package br.com.Leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {

    public Leilao() {
        lances = new ArrayList<>();
    }

    private List<Lance> lances;

    public List<Lance> getListaLances() {
        return lances;
    }

    public void setListaLances(List<Lance> lance) {
        this.lances = lances;
    }

    public void adicionarNovoLance(Lance lance){
        validarNovoLance(lance);
        lances.add(lance);
    }

    public void validarNovoLance(Lance lance){
        if(lances.get(0).getValorDoLance() > 0){
           throw new RuntimeException("Deverá haver um lance valido ");
        }
    }
}
