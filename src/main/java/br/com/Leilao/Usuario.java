package br.com.Leilao;

public class Usuario {

    private int id;
    private String nome;

    public Usuario(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void validaEntrada(){
        if(this.id == 0 || this.id < 0)
        {
            throw new RuntimeException("Identificador tem que ser maior que zeros");
        }
        if (this.nome.isEmpty()){
            throw new RuntimeException("O nome tem que ter uma valor valido");
        }
    }

}

