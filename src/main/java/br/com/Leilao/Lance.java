package br.com.Leilao;

public class Lance {

    private Usuario usuario;
    private double valorDoLance;

    public Lance(Usuario usuario, double valorDoLance) {
        this.usuario = usuario;
        this.valorDoLance = valorDoLance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }

    public void validaEntradaLance(){
        if (this.usuario.toString().isEmpty()){
            throw new RuntimeException("Nome não preenchido");
        }
        if (this.valorDoLance ==0 || this.valorDoLance < 0){
            throw new RuntimeException("Valor do lançe deverá ser maior que zero");
        }
    }

}
